const BASE_URL = "https://635f4b1c3e8f65f283b012a0.mockapi.io";

var idEdited = null;

function fetchAllTodo() {
  //render all todos services
  turnOnLoading();
  axios({
    url: `${BASE_URL}/todos`,
    method: "GET",
  })
    //Nếu thành công ~ dùng .then
    .then(function (res) {
      // console.log("res todos: ", res.data);
      turnOffLoading();
      renderTodoList(res.data);
    })
    // Nếu thất bại ~ dùng .catch
    .catch(function (err) {
      turnOffLoading();
      console.log("err: ", err);
    });
}
// chạy lần đầu khi load trang
fetchAllTodo();

//Remove all todos
function removeTodo(idTodo) {
  turnOnLoading();
  axios({
    url: `${BASE_URL}/todos/${idTodo}`,
    method: "DELETE",
  })
    .then(function (res) {
      turnOffLoading();
      // gọi lại API get all todos
      fetchAllTodo();
      console.log("res: ", res);
    })
    .catch(function (err) {
      turnOffLoading();
      console.log("err: ", err);
    });
}

// Add Todos
function addTodo() {
  var data = layThongTinTuform();
  var newTodo = {
    name: data.name,
    desc: data.desc,
    isComplete: true, // biến mới khi thêm vào sẽ là chưa hoàn thành
  };
  turnOffLoading();
  axios({
    url: `${BASE_URL}/todos`,
    method: "POST",
    data: newTodo,
  })
    //Nếu thành công ~ dùng .then
    .then(function (res) {
      // thêm vào để clear giá trị sau khi thêm giá trị vào
      document.getElementById("name").value = "";
      document.getElementById("desc").value = "";

      // console.log("res todos: ", res.data);
      turnOffLoading();
      fetchAllTodo();
      console.log("res: ", res);
    })
    // Nếu thất bại ~ dùng .catch
    .catch(function (err) {
      turnOffLoading();
      console.log("err: ", err);
    });
}



function editTodo(idTodo) {
  axios({
    url: `${BASE_URL}/todos/${idTodo}`,
    method: "GET",
  })
  .then(function(res){
    // show thông tin lên form
    document.getElementById("name").value = res.data.name;
    document.getElementById("desc").value = res.data.desc;
    idEdited = res.data.id;
  })
  .catch(function (err){
    turn0ffLoading();
    console.log('err: ', err);
  })
}

function updateTodo() {
  let data = layThongTinTuform();
  axios({
    url: `${BASE_URL}/todos/&{idEdited}`,
    method: "PUT",
    data: data,
  })
  .then(function (res){
    console.log('res: ', res);
    fetchAllTodo();
  })
  .catch(function (err){
    console.log('err: ', err);
  });
}