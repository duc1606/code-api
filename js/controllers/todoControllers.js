function renderTodoList(todos) {
  var contentHTML = "";
  todos.forEach(function (item) {
    var contentTr = `<tr>
        <td>${item.id}</td>
        <td>${item.name}</td>
        <td>${item.desc}</td>
        <td>
          <input type="checkbox" ${item.isComplete ? "checked" : ""}/>
        </td>
        <td>
          <button class="btn btn-danger" onclick="removeTodo(${
            item.id
          })">Delete</button>
        </td>
        <td><button onclick="editTodo(${
          item.id
        })" class="btn btn-success">sửa</button></td>
        </tr>`;
    contentHTML += contentTr;
  });
  document.getElementById("tbody-todos").innerHTML = contentHTML;
}

function turnOnLoading() {
  document.getElementById("loading").style.display = "flex";
}

function turnOffLoading() {
  document.getElementById("loading").style.display = "none";
}

function layThongTinTuform() {
  var name = document.getElementById("name").value;
  var desc = document.getElementById("desc").value;

  return {
    name: name,
    desc: desc,
  };
}
